package co.com.enginebi.certification.interactions;

import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.WebElement;

import java.util.List;

public class ChooseElement implements Interaction {

    private Target element;
    private String nameElement;

    public ChooseElement(Target element, String nameElement){
        this.element = element;
        this.nameElement = nameElement;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        List<WebElementFacade> optionsElements = this.element.resolveAllFor(actor);
        for (WebElement optionElements : optionsElements) {
            if(optionElements.getText().equalsIgnoreCase(nameElement)){
                optionElements.click();
                break;
            }
        }
    }
    public static ChooseElement web(Target element, String nameElement){
        return new ChooseElement(element, nameElement);
    }
}
