package co.com.enginebi.certification.utils;

import java.util.List;
import java.util.Map;

import static co.com.enginebi.certification.enums.ExcelKey.PATH_FILE;
import static co.com.enginebi.certification.enums.ExcelKey.QUERY_EXCEL;

public class SetData {

    private SetData(){}

    public static List<Map<String, String>> test(String sheet, String scenario){
        List<Map<String, String>> datos;
        datos = GetData.deExcel(PATH_FILE.getVariable(), String.format(QUERY_EXCEL.getVariable(), sheet, scenario));
        return datos;
    }
}
