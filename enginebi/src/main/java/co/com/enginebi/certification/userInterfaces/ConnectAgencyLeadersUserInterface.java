package co.com.enginebi.certification.userInterfaces;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class ConnectAgencyLeadersUserInterface extends PageObject {

    public static final Target FIRST_NAME_INPUT = Target.the("First name input").located(By.id("firstname"));
    public static final Target LAST_NAME_INPUT = Target.the("Last name input").located(By.id("lastname"));
    public static final Target NUMBER_ADDITIONAL_GUESTS_INPUT = Target.the("Number additional guests").located(By.id("field[36]"));
    public static final Target EMAIL_INPUT = Target.the("email input").located(By.id("email"));
    public static final Target COMPANY_NAME_INPUT = Target.the("Company name input").located(By.id("field[1]"));
    public static final Target HEAR_EVENT_NAME = Target.the("Hear event input").located(By.id("field[38]"));
    public static final Target SUBMIT_BUTTON = Target.the("Submit button").located(By.id("_form_16_submit"));
    public static final Target MESSAGE_CONNECT_AGENCY_LABEL = Target.the("Message choose format label").located(By.className("elementor-size-default"));
    public static final Target MANDATORY_FIELD_LABEL = Target.the("Input Form").located(By.className("_error-inner"));

}
