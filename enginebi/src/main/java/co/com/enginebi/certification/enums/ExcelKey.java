package co.com.enginebi.certification.enums;

public enum ExcelKey {

    PATH_FILE("./src/test/resources/datatables/enginebiDataFile.xlsx"),
    QUERY_EXCEL("select * from  %s where idTest =  %s"),
    REGISTER_SUITS("Register"),
    LOGIN_SUCCESSFUL("LoginSuccessful"),
    CONNECT_AGENCY_LEADERS("ConnectAgencyLeaders"),
    NAME_EXCEL("name"),
    LAST_NAME_EXCEL("lastname"),
    EMAIL_REGISTERED("email"),
    PASSWORD_REGISTERED("password"),
    COMPANY_NAME_EXCEL("Company Name"),
    START_FISCAL_EXCEL("When do you start your fiscal year"),
    ACCOUNT_SOFTWARE_EXCEL("Your Accounting Software"),
    COUNTRY_EXCEL("Country"),
    STATE_EXCEL("State"),
    CITY_EXCEL("City"),
    NUMBER_PHONE("numberPhone")
    ;

    private String variable;

    ExcelKey(String variable) {
        this.variable = variable;
    }

    public String getVariable() {
        return variable;
    }
}
