package co.com.enginebi.certification.enums;

public enum SystemKey {

    URL_HOME_PAGE("url_home_page"),
    USER("user"),
    SUBSCRIPTION_PURCHASE_ORDER_TERMS("Subscription Purchase Order Terms"),
    SOFTWARE_SERVICE_TERMS_CONDITION("Software and Service Terms and Conditions"),
    NAME_REFERENCE_TEXT_LABEL("xecution"),
    DOWNLOAD_EBOOK_LABEL("Download Our eBook"),
    TEXT_REFERENCE("Join Us at the next Chicago Agency Happy Hour"),
    TEXT_REFERENCE_II("RSVP NOW"),

    ;
    private String words;
    SystemKey(String words) {
        this.words = words;
    }
    public String getWords() {
        return words;
    }

}
