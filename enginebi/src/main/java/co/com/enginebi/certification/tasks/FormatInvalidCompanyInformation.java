package co.com.enginebi.certification.tasks;

import co.com.enginebi.certification.interactions.ChooseElement;
import co.com.enginebi.certification.interactions.Click;
import co.com.enginebi.certification.interactions.Type;
import co.com.enginebi.certification.models.DataExcel;
import net.serenitybdd.annotations.Shared;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Scroll;

import java.util.Map;

import static co.com.enginebi.certification.enums.ExcelKey.*;
import static co.com.enginebi.certification.userInterfaces.CompanyInformationUserInterface.*;

public class FormatInvalidCompanyInformation implements Task {

    @Shared
    DataExcel dataExcel;

    @Override
    public <T extends Actor> void performAs(T actor) {

        Map<String, String> data = dataExcel.getSetDeDatos().get(0);
        actor.attemptsTo(Type.on(COMPANY_NAME_INPUT, data.get(COMPANY_NAME_EXCEL.getVariable())));
        actor.attemptsTo(Click.on(START_YEAR_FISCAL_LIST));
        actor.attemptsTo(ChooseElement.web(LIST_OPTIONS, data.get(START_FISCAL_EXCEL.getVariable())));
        actor.attemptsTo(Click.on(ACCOUNTING_SOFTWARE_LIST));
        actor.attemptsTo(ChooseElement.web(LIST_OPTIONS, data.get(ACCOUNT_SOFTWARE_EXCEL.getVariable())));
        actor.attemptsTo(Click.on(COUNTRY_LIST));
        actor.attemptsTo(ChooseElement.web(LIST_OPTIONS, data.get(COUNTRY_EXCEL.getVariable())));
        actor.attemptsTo(Click.on(STATE_LIST));
        actor.attemptsTo(ChooseElement.web(LIST_OPTIONS, data.get(STATE_EXCEL.getVariable())));
        actor.attemptsTo(Type.on(CITY_INPUT, data.get(CITY_EXCEL.getVariable())));
        actor.attemptsTo(Click.on(TERMS_CONDITIONS_CHECK_BOX));
        actor.attemptsTo(Scroll.to(COMPANY_INFORMATION_NEXT_BUTTON));
    }

    public static FormatInvalidCompanyInformation form() {
        return Tasks.instrumented(FormatInvalidCompanyInformation.class);
    }
}
