package co.com.enginebi.certification.enums;

public enum TestKey {

    FREE_REGISTER_SUCCESSFUL_TEST("1"),
    LOGIN_SUCCESSFUL_TEST("1"),
    LOGIN_WRONG_TEST("2"),
    CONNECT_AGENCY_LEADERS_TEST("1")

    ;

    private String testNumber;
    TestKey(String testNumber){
        this.testNumber = testNumber;
    }
    public String getTestNumber() {
        return testNumber;
    }
}
