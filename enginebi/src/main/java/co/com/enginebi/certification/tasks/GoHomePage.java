package co.com.enginebi.certification.tasks;

import co.com.enginebi.certification.interactions.Click;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;

import static co.com.enginebi.certification.userInterfaces.HomePageUserInterface.LOGO_HEADER_BUTTON;

public class GoHomePage implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(LOGO_HEADER_BUTTON));
    }
    public static GoHomePage webside(){
        return Tasks.instrumented(GoHomePage.class);
    }
}
