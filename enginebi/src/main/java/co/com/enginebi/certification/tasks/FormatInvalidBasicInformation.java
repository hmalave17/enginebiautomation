package co.com.enginebi.certification.tasks;

import co.com.enginebi.certification.interactions.Type;
import co.com.enginebi.certification.models.DataExcel;
import net.serenitybdd.annotations.Shared;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Scroll;

import java.util.Map;

import static co.com.enginebi.certification.enums.ExcelKey.*;
import static co.com.enginebi.certification.userInterfaces.BasicInformationUserInterface.*;
import static co.com.enginebi.certification.userInterfaces.BasicInformationUserInterface.BUSINESS_PHONE_NUMBER_INPUT;

public class FormatInvalidBasicInformation implements Task {

    @Shared
    DataExcel dataExcel;

    @Override
    public <T extends Actor> void performAs(T actor) {
        Map<String, String> data = dataExcel.getSetDeDatos().get(0);
        actor.attemptsTo(Type.on(FIRST_NAME_INPUT, data.get(NAME_EXCEL.getVariable())));
        actor.attemptsTo(Type.on(LAST_NAME_INPUT, data.get(LAST_NAME_EXCEL.getVariable())));
        actor.attemptsTo(Type.on(EMAIL_ADDRESS_INPUT, data.get(EMAIL_REGISTERED.getVariable())));
        actor.attemptsTo(Type.on(BUSINESS_PHONE_NUMBER_INPUT, data.get(NUMBER_PHONE.getVariable())));
        actor.attemptsTo(Scroll.to(BASIC_INFORMATION_NEXT_BUTTON));
    }
    public static FormatInvalidBasicInformation form(){
        return Tasks.instrumented(FormatInvalidBasicInformation.class);
    }
}
