package co.com.enginebi.certification.tasks;

import co.com.enginebi.certification.interactions.ChooseElement;
import co.com.enginebi.certification.interactions.Click;
import co.com.enginebi.certification.interactions.Scroll;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.targets.Target;



import static co.com.enginebi.certification.userInterfaces.HomePageUserInterface.ACCEPT_COOKIES_BUTTON;


public class SelectOptionHome implements Task {

    private Target reference;
    private String textReference;
    private Target option;
    private String textMenu;

    public SelectOptionHome(Target reference, String textReference, Target option, String textMenu){
        this.reference = reference;
        this.textReference = textReference;
        this.option = option;
        this.textMenu = textMenu;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(ACCEPT_COOKIES_BUTTON));
        actor.attemptsTo(Scroll.to(reference, textReference));
        actor.attemptsTo(ChooseElement.web(option, textMenu));
    }
    public static SelectOptionHome page(Target reference, String textReference, Target option, String textMenu){
        return Tasks.instrumented(SelectOptionHome.class, reference, textReference, option, textMenu);
    }
}
