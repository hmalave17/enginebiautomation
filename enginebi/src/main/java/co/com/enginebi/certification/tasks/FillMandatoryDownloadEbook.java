package co.com.enginebi.certification.tasks;

import co.com.enginebi.certification.interactions.Click;
import co.com.enginebi.certification.interactions.Type;
import co.com.enginebi.certification.utils.NumberRamdom;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Clear;

import static co.com.enginebi.certification.userInterfaces.DownloadEbookUserIntarface.*;
import static co.com.enginebi.certification.userInterfaces.DownloadEbookUserIntarface.DOWNLOAD_BUTTON;

public class FillMandatoryDownloadEbook implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Type.on(EMAIL_INPUT, NumberRamdom.email()));
        actor.attemptsTo(Clear.field(EMAIL_INPUT));
        actor.attemptsTo(Click.on(DOWNLOAD_BUTTON));
    }
    public static FillMandatoryDownloadEbook input(){
        return Tasks.instrumented(FillMandatoryDownloadEbook.class);
    }
}
