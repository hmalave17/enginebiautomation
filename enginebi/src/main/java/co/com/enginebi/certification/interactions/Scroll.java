package co.com.enginebi.certification.interactions;

import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.WebElement;


import java.util.List;

public class Scroll implements Task {

    private Target element;
    private String textElement;

    public Scroll(Target element, String textElement){
        this.element =element;
        this.textElement = textElement;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        List<WebElementFacade> optionsElements = this.element.resolveAllFor(actor);
        for (WebElement optionElements : optionsElements) {
            if(optionElements.getText().equalsIgnoreCase(textElement)){
                actor.attemptsTo(net.serenitybdd.screenplay.actions.Scroll.to(optionElements));
                break;
            }
        }
    }
    public static Scroll to(Target element, String textElement){
        return Tasks.instrumented(Scroll.class, element, textElement);
    }
}
