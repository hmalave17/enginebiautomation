package co.com.enginebi.certification.userInterfaces;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class CompanyInformationUserInterface extends PageObject {

    public static final Target COMPANY_NAME_INPUT = Target.the("Company name input").located(By.id("customerName"));
    public static final Target START_YEAR_FISCAL_LIST = Target.the("Start year fiscal list").located(By.id("fiscalMonth"));
    public static final Target LIST_OPTIONS = Target.the("Start year fiscal list").located(By.className("ant-select-item-option-content"));
    public static final Target ACCOUNTING_SOFTWARE_LIST = Target.the("Accounting software list").located(By.id("accountingSoftware"));
    public static final Target COUNTRY_LIST = Target.the("Country list").located(By.id("countryId"));
    public static final Target STATE_LIST = Target.the("State list").located(By.id("stateId"));
    public static final Target CITY_INPUT = Target.the("City input").located(By.id("city"));
    public static final Target TERMS_CONDITIONS_CHECK_BOX = Target.the("Terms conditions check box").located(By.className("ant-checkbox"));
    public static final Target COMPANY_INFORMATION_NEXT_BUTTON = Target.the("Next button").located(By.className("btn-next"));
    public static final Target LEGAL_TERMS_BUTTON = Target.the("Legal terms button").located(By.className("ng-tns-c61-17"));
    public static final Target SUBSCRIPTIONS_BUTTON = Target.the("subscriptions button").located(By.className("elementor-widget-container"));
    public static final Target SOFTWARE_SERVICES_BUTTON = Target.the("Software services button").located(By.className("elementor-heading-title"));
    public static final Target FORM_NAME_LABEL = Target.the("Form name label").located(By.className("statusName"));
    public static final Target ERROR_FORMAT_LABEL = Target.the("Company information input").located(By.className("ant-form-item-explain-error"));
}
