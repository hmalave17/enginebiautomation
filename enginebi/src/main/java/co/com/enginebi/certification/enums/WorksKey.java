package co.com.enginebi.certification.enums;

public enum WorksKey {

    PRICING("Pricing"),
    FREE_TRIAL("Free Trial"),

    ;

    private String work;

    WorksKey(String work){
        this.work = work;
    }

    public String getWork() {
        return work;
    }
}
