package co.com.enginebi.certification.exceptions;

import static co.com.enginebi.certification.enums.SystemMessage.NOT_DATA;

public class DoesNotFindData extends AssertionError{
    public DoesNotFindData(){
        super(NOT_DATA.getMessage());
    }
}
