package co.com.enginebi.certification.tasks;

import co.com.enginebi.certification.interactions.ChooseElement;
import co.com.enginebi.certification.interactions.Scroll;
import co.com.enginebi.certification.interactions.SwitchTabWindow;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.targets.Target;

public class VerifyLegalInformation implements Task {

    private Target element;
    private String textElement;

    public VerifyLegalInformation(Target element, String textElement){
        this.element = element;
        this.textElement = textElement;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Scroll.to(element, textElement));
        actor.attemptsTo(ChooseElement.web(element, textElement));
        actor.attemptsTo(SwitchTabWindow.change());
    }
    public static VerifyLegalInformation user(Target element, String textElement){
        return Tasks.instrumented(VerifyLegalInformation.class, element, textElement);
    }
}
