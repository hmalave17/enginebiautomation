package co.com.enginebi.certification.userInterfaces;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class HomePageUserInterface extends PageObject {

    public static final Target LOGO_HEADER_BUTTON = Target.the("Logo header button").located(By.className("logo-brand"));
    public static final Target HOME_PAGE_BIG_BUTTON = Target.the("Login engine bi button").located(By.className("elementor-button-text"));
    public static final Target PRINCIPAL_MENU_OPTIONS = Target.the("Principal menu options").located(By.className("hfe-menu-item"));
    public static final Target FREE_REGISTER_BUTTON = Target.the("Free register button").located(By.className("hfe-sub-menu-item"));
    public static final Target START_FREE_REGISTER_BUTTON = Target.the("Start register button").located(By.className("btn-free-trial"));
    public static final Target START_PAID_REGISTER_BUTTON = Target.the("Start register button").located(By.className("ant-btn-primary"));
    public static final Target REFERENCE_TEXT_LABEL = Target.the("Reference text label").located(By.className("elementor-icon-list-icon"));
    public static final Target ACCEPT_COOKIES_BUTTON = Target.the("Accept cookies button").located(By.className("wt-cli-accept-all-btn"));
    public static final Target SECOND_REFERENCE_TEXT_LABEL = Target.the("Reference text label").located(By.className("elementor-element-754e3394"));

}
