package co.com.enginebi.certification.tasks;

import co.com.enginebi.certification.interactions.ChooseElement;
import co.com.enginebi.certification.interactions.Click;
import co.com.enginebi.certification.interactions.MoveToWebElement;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.WebDriver;

import static co.com.enginebi.certification.enums.WorksKey.PRICING;
import static co.com.enginebi.certification.userInterfaces.HomePageUserInterface.*;

public class SelectOptionRegister implements Task {

    private WebDriver webDriver;
    private String plan;
    private Target optionRegister;

    public SelectOptionRegister(WebDriver webDriver, String plan, Target optionRegister){
        this.webDriver = webDriver;
        this.plan = plan;
        this.optionRegister = optionRegister;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(MoveToWebElement.withMouse(PRINCIPAL_MENU_OPTIONS, PRICING.getWork(), webDriver));
        actor.attemptsTo(ChooseElement.web(FREE_REGISTER_BUTTON, plan));
        actor.attemptsTo(Scroll.to(optionRegister));
        actor.attemptsTo(Click.on(optionRegister));
    }
    public static SelectOptionRegister user(WebDriver webDriver, String plan, Target optionRegister){
        return Tasks.instrumented(SelectOptionRegister.class, webDriver, plan, optionRegister);
    }
}
