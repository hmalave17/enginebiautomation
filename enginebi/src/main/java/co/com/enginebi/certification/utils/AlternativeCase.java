package co.com.enginebi.certification.utils;

public class AlternativeCase {

    private static final String EMAIL = "email";

    private AlternativeCase() {
    }

    public static Integer login(String feature) {
        if (feature.equalsIgnoreCase(EMAIL)){
            return 0;
        } else {
            return 1;
        }
    }

    public static String loginInputName(String feature){
        if (feature.equalsIgnoreCase(EMAIL)){
            return "Username";
        } else {
            return "Password";
        }
    }

    public static Integer basicInformation(String feature){
        int result;
        switch (feature){
            case "First Name":
                result = 0;
                break;
            case "Last Name":
                result = 1;
                break;
            case "Email Address":
                result = 2;
                break;
            default:
                result = 3;
                break;
        }
        return result;
    }
}
