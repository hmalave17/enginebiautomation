package co.com.enginebi.certification.utils;

import java.security.SecureRandom;

public class NumberRamdom {

    private NumberRamdom(){}

    public static String businessPhoneNumber() {
        SecureRandom random = new SecureRandom();
        long min = 100000000L;
        long max = 999999999L;
        long phoneRandom = min + ((long) (random.nextDouble() * (max - min)));
        return "3" + Long.toString(phoneRandom);
    }

    public static String email() {
        SecureRandom random = new SecureRandom();
        long min = 1000L;
        long max = 9999L;
        long emailRandom = min + ((long) (random.nextDouble() * (max - min)));
        return "TestAutomate" + Long.toString(emailRandom) + "@enginebi.com";
    }
}
