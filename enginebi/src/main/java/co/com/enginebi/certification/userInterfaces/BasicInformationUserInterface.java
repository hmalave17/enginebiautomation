package co.com.enginebi.certification.userInterfaces;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class BasicInformationUserInterface extends PageObject {

    public static final Target FIRST_NAME_INPUT = Target.the("First name input").located(By.id("name"));
    public static final Target LAST_NAME_INPUT = Target.the("Last name input").located(By.id("lastName"));
    public static final Target EMAIL_ADDRESS_INPUT = Target.the("email address input").located(By.id("email"));
    public static final Target BUSINESS_PHONE_NUMBER_INPUT = Target.the("Business phone number input").located(By.id("phone"));
    public static final Target BASIC_INFORMATION_NEXT_BUTTON = Target.the("Next button").located(By.className("anticon-right"));
    public static final Target BASIC_INFORMATION_INPUT = Target.the("basic information input").located(By.className("ant-input-status-success"));

}
