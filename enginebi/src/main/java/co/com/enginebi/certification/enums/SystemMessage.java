package co.com.enginebi.certification.enums;

public enum SystemMessage {

    URL_DOESNOT_DEFINE("URL home page is not set or is empty in serenity.properties"),
    NOT_DATA("Doesn't find data"),
    LOGIN_WELCOME("Welcome,"),
    LOGIN_WRONG("Error\nInvalid login attempt."),
    LOGIN_MANDATORY_FILL("Error\nThe %S field is required.\nNot valid login model state."),
    LEGAL_INFORMATION_PURCHASE_ORDER("EngineBI\nSubscription Purchase Order Terms"),
    LEGAL_INFORMATION_TERMS_SERVICE("EngineBI\nSoftware and Service Terms and Conditions"),
    COMPANY_INFORMATION("Company Information"),
    FIELD_MANDATORY("This field is required."),
    INVALID_EMAIL("Enter a valid email address."),
    CHOOSE_FORMAT_EBOOK("Choose your ebook format"),
    CONNECT_AGENCY_SUCCESSFUL("Thank you for Signing up for our next Chicago Agency Happy Hour occurring on Jun 20, 2024"),
    ;


    private String message;

    SystemMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
