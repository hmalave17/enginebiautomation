package co.com.enginebi.certification.tasks;

import co.com.enginebi.certification.interactions.ChooseElement;
import co.com.enginebi.certification.interactions.Click;
import co.com.enginebi.certification.interactions.SwitchTabWindow;
import co.com.enginebi.certification.interactions.Type;
import co.com.enginebi.certification.models.DataExcel;
import co.com.enginebi.certification.utils.ValidateEmpty;
import net.serenitybdd.annotations.Shared;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Clear;

import java.util.Map;

import static co.com.enginebi.certification.enums.ExcelKey.EMAIL_REGISTERED;
import static co.com.enginebi.certification.enums.ExcelKey.PASSWORD_REGISTERED;
import static co.com.enginebi.certification.userInterfaces.HomePageUserInterface.HOME_PAGE_BIG_BUTTON;
import static co.com.enginebi.certification.userInterfaces.LoginPageUserInterface.*;

public class FillMandatoyLogin implements Task {

    private Integer input;

    public FillMandatoyLogin(Integer input){
        this.input = input;
    }

    @Shared
    DataExcel dataExcel;

    @Override
    public <T extends Actor> void performAs(T actor) {
        Map<String, String> data = dataExcel.getSetDeDatos().get(0);
        actor.attemptsTo(ChooseElement.web(HOME_PAGE_BIG_BUTTON, "Log Into EngineBI"));
        actor.attemptsTo(SwitchTabWindow.change());
        actor.attemptsTo(Type.on(EMAIL_ADDRESS_INPUT, ValidateEmpty.email(data.get(EMAIL_REGISTERED.getVariable()))));
        actor.attemptsTo(Type.on(PASSWORD_INPUT, ValidateEmpty.password(data.get(PASSWORD_REGISTERED.getVariable()))));
        actor.attemptsTo(Click.on(REFERENCE_CARD));
        actor.attemptsTo(Clear.field(LOGIN_INPUT.resolveAllFor(actor).get(input)));
        actor.attemptsTo(Click.on(SIGN_IN_BUTTON));

    }
    public static FillMandatoyLogin input(Integer input){
        return Tasks.instrumented(FillMandatoyLogin.class, input);
    }
}
