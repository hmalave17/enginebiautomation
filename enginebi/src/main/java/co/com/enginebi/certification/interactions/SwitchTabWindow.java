package co.com.enginebi.certification.interactions;

import java.util.ArrayList;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;


public class SwitchTabWindow implements Interaction{

    @Override
    public <T extends Actor> void performAs(T actor) {
        ArrayList<String> tab = new ArrayList<>(BrowseTheWeb.as(actor).getDriver().getWindowHandles());
        BrowseTheWeb.as(actor).getDriver().switchTo().window(tab.get(1));

    }
    public static SwitchTabWindow change () {
        return Tasks.instrumented(SwitchTabWindow.class);
    }


}