package co.com.enginebi.certification.userInterfaces;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class LoginPageUserInterface extends PageObject {

    public static final Target EMAIL_ADDRESS_INPUT = Target.the("email address input").located(By.id("email-input"));
    public static final Target PASSWORD_INPUT = Target.the("Password input").located(By.name("Password"));
    public static final Target REFERENCE_CARD= Target.the("Card reference").located(By.className("ribbon-holder"));
    public static final Target SIGN_IN_BUTTON = Target.the("Sign in input").located(By.id("loginButton"));
    public static final Target WELCOME_LABEL = Target.the("Welcome label").located(By.className("home-user"));
    public static final Target ALERT_LABEL = Target.the("Alert label").located(By.className("alert-danger"));
    public static final Target LOGIN_INPUT = Target.the("login input").located(By.className("form-control"));
}
