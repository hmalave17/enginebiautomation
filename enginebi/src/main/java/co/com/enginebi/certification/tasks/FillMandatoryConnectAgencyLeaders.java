package co.com.enginebi.certification.tasks;

import co.com.enginebi.certification.interactions.Click;
import co.com.enginebi.certification.interactions.Type;
import co.com.enginebi.certification.models.DataExcel;
import co.com.enginebi.certification.utils.NumberRamdom;
import net.serenitybdd.annotations.Shared;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Clear;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

import java.util.Map;

import static co.com.enginebi.certification.enums.ExcelKey.LAST_NAME_EXCEL;
import static co.com.enginebi.certification.enums.ExcelKey.NAME_EXCEL;
import static co.com.enginebi.certification.userInterfaces.ConnectAgencyLeadersUserInterface.*;

public class FillMandatoryConnectAgencyLeaders implements Task {

    private String input;

    public FillMandatoryConnectAgencyLeaders(String input) {
        this.input = input;
    }

    @Shared
    DataExcel dataExcel;

    @Override
    public <T extends Actor> void performAs(T actor) {

        Map<String, String> data = dataExcel.getSetDeDatos().get(0);
        actor.attemptsTo(Type.on(FIRST_NAME_INPUT, data.get(NAME_EXCEL.getVariable())));
        actor.attemptsTo(Type.on(LAST_NAME_INPUT, data.get(LAST_NAME_EXCEL.getVariable())));
        actor.attemptsTo(Type.on(NUMBER_ADDITIONAL_GUESTS_INPUT, NumberRamdom.businessPhoneNumber()));
        actor.attemptsTo(Type.on(EMAIL_INPUT, NumberRamdom.email()));
        actor.attemptsTo(Type.on(COMPANY_NAME_INPUT, data.get(NAME_EXCEL.getVariable())));
        actor.attemptsTo(SelectFromOptions.byVisibleText("Directly from Jon Morris").from(HEAR_EVENT_NAME));

        if ("first name".equalsIgnoreCase(input)) {
            actor.attemptsTo(Clear.field(FIRST_NAME_INPUT));
        } else if ("number additional".equalsIgnoreCase(input)) {
            actor.attemptsTo(Clear.field(NUMBER_ADDITIONAL_GUESTS_INPUT));
        } else if ("email".equalsIgnoreCase(input)) {
            actor.attemptsTo(Clear.field(EMAIL_INPUT));
        } else {
            actor.attemptsTo(Clear.field(COMPANY_NAME_INPUT));
        }
        actor.attemptsTo(Click.on(SUBMIT_BUTTON));
    }

    public static FillMandatoryConnectAgencyLeaders form(String input) {
        return Tasks.instrumented(FillMandatoryConnectAgencyLeaders.class, input);
    }
}
