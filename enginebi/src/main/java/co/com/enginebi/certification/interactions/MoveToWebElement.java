package co.com.enginebi.certification.interactions;

import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

public class MoveToWebElement implements Interaction {

    private Target element;
    private String nameElement;
    private WebDriver webDriver;

    public MoveToWebElement(Target element, String nameElement, WebDriver webDriver){
        this.element = element;
        this.nameElement = nameElement;
        this.webDriver = webDriver;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        List<WebElementFacade> optionsElements = this.element.resolveAllFor(actor);
        for (WebElement optionElements : optionsElements) {
            if(optionElements.getText().equalsIgnoreCase(nameElement)){
                Actions actions = new Actions(webDriver);
                actions.moveToElement(optionElements).build().perform();
                break;
            }
        }
    }
    public static MoveToWebElement withMouse(Target element, String nameElement, WebDriver webDriver){
        return new MoveToWebElement(element, nameElement, webDriver);
    }
}
