package co.com.enginebi.certification.exceptions;

public class UnexpectedBehavior extends Exception {

    public static final String UNEXPECTED_MESSAGE = "The behavior unexpected";
    public UnexpectedBehavior(String message, Throwable cause){
        super(message, cause);
    }
}
