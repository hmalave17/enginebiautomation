package co.com.enginebi.certification.questions;

import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.WebElement;

import java.util.List;



public class ValidateMessage implements Question<Boolean> {

    private Target element;
    private String message;

    public ValidateMessage(Target element, String message) {
        this.element = element;
        this.message = message;
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        List<WebElementFacade> namesElements = this.element.resolveAllFor(actor);
        for (WebElement nameElement : namesElements) {
            if (nameElement.getText().equalsIgnoreCase(message)) {
                return true;
            }
        }
        return false;
    }
    public static ValidateMessage element(Target element, String message) {
        return new ValidateMessage(element, message);
    }
}
