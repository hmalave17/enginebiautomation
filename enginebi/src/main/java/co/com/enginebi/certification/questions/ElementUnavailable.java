package co.com.enginebi.certification.questions;

import co.com.enginebi.certification.utils.Log;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.targets.Target;

import java.util.logging.Level;


public class ElementUnavailable implements Question<Boolean> {

    private Target element;

    public ElementUnavailable(Target element){this.element = element; }

    @Override
    public Boolean answeredBy(Actor actor) {
        boolean result = true;
        if(element.resolveFor(actor).isClickable()){
            Log.getLogger().log(Level.INFO, "========================= the element es Clickable =========================");
            result = false;
        }
        return result;
    }
    public static ElementUnavailable click(Target element){
        return new ElementUnavailable(element);
    }
}
