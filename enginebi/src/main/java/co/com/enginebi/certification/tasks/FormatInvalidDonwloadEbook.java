package co.com.enginebi.certification.tasks;

import co.com.enginebi.certification.interactions.Click;
import co.com.enginebi.certification.interactions.Type;
import co.com.enginebi.certification.models.DataExcel;
import net.serenitybdd.annotations.Shared;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;

import java.util.Map;

import static co.com.enginebi.certification.enums.ExcelKey.EMAIL_REGISTERED;
import static co.com.enginebi.certification.userInterfaces.DownloadEbookUserIntarface.*;
import static co.com.enginebi.certification.userInterfaces.DownloadEbookUserIntarface.DOWNLOAD_BUTTON;

public class FormatInvalidDonwloadEbook implements Task {

    @Shared
    DataExcel dataExcel;

    @Override
    public <T extends Actor> void performAs(T actor) {
        Map<String, String> data = dataExcel.getSetDeDatos().get(0);
        actor.attemptsTo(Type.on(EMAIL_INPUT, data.get(EMAIL_REGISTERED.getVariable())));
        actor.attemptsTo(Click.on(DOWNLOAD_BUTTON));
    }
    public static FormatInvalidDonwloadEbook form(){
        return Tasks.instrumented(FormatInvalidDonwloadEbook.class);
    }
}
