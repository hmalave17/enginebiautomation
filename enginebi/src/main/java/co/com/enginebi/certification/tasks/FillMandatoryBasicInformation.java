package co.com.enginebi.certification.tasks;

import co.com.enginebi.certification.interactions.Click;
import co.com.enginebi.certification.interactions.Type;
import co.com.enginebi.certification.models.DataExcel;
import co.com.enginebi.certification.utils.NumberRamdom;
import net.serenitybdd.annotations.Shared;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Clear;

import java.util.Map;

import static co.com.enginebi.certification.enums.ExcelKey.LAST_NAME_EXCEL;
import static co.com.enginebi.certification.enums.ExcelKey.NAME_EXCEL;
import static co.com.enginebi.certification.userInterfaces.BasicInformationUserInterface.*;
import static co.com.enginebi.certification.userInterfaces.LoginPageUserInterface.LOGIN_INPUT;

public class FillMandatoryBasicInformation implements Task {

    private Integer input;

    public FillMandatoryBasicInformation(Integer input){
        this.input = input;
    }

    @Shared
    DataExcel dataExcel;

    @Override
    public <T extends Actor> void performAs(T actor) {
        Map<String, String> data = dataExcel.getSetDeDatos().get(0);
        actor.attemptsTo(Type.on(FIRST_NAME_INPUT, data.get(NAME_EXCEL.getVariable())));
        actor.attemptsTo(Type.on(LAST_NAME_INPUT, data.get(LAST_NAME_EXCEL.getVariable())));
        actor.attemptsTo(Type.on(EMAIL_ADDRESS_INPUT, NumberRamdom.email()));
        actor.attemptsTo(Type.on(BUSINESS_PHONE_NUMBER_INPUT, NumberRamdom.businessPhoneNumber()));
        actor.attemptsTo(Clear.field(BASIC_INFORMATION_INPUT.resolveAllFor(actor).get(input)));
    }

    public static FillMandatoryBasicInformation form(Integer input){
        return Tasks.instrumented(FillMandatoryBasicInformation.class, input);
    }
}
