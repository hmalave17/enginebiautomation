package co.com.enginebi.certification.exceptions;

public class UnexpectedMessage extends Exception{
    public static final String UNEXPECTED_MESSAGE = "The message unexpected message";

    public UnexpectedMessage(String message, Throwable cause){
        super(message, cause);
    }
}
