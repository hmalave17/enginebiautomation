package co.com.enginebi.certification.userInterfaces;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class DownloadEbookUserIntarface extends PageObject {

    public static final Target FIRST_NAME_INPUT = Target.the("First name input").located(By.id("firstname"));
    public static final Target LAST_NAME_INPUT = Target.the("Last name input").located(By.id("lastname"));
    public static final Target EMAIL_INPUT = Target.the("email input").located(By.id("email"));
    public static final Target DOWNLOAD_BUTTON = Target.the("Download button").located(By.id("_form_5_submit"));
    public static final Target MESSAGE_CHOOSE_FORMAT_LABEL = Target.the("Message choose format label").located(By.className("elementor-element-df8b8cd"));
    public static final Target ALERT_LABEL = Target.the("Alert label").located(By.className("_error-inner"));

}
