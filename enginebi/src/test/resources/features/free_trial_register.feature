#Language: en
#Author: Hernan Malave

@RegressionTest
Feature: Free user registration

  As a Product Owner,
  I want users to be able to register for free
  So that they can get to know the product

  Background:
    Given user is on the website home page

  @RegisterFreeSuccessful
  Scenario: User registers for free
    Given the user selects the free registration option
    When the user fills out the registration form
    Then the system registers the user for free

  @SubscriptionPurchaseOrderTerms
  Scenario: verify Subscription Purchase Order Terms
    Given the user selects the free registration option
    When user review Subscription Purchase Order Terms
    Then System show Subscription Purchase Order Terms information

  @SoftwareServiceTermsConditions
  Scenario: verify Software and Service Terms and Conditions
    Given the user selects the free registration option
    When user review Software and Service Terms and Conditions
    Then System show Software and Service Terms and Conditions information
