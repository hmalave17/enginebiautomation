#Language: en
#Author: Hernan Malave

@RegressionTest
Feature: user Connect Agency Leaders

  As a Product Owner,
  I want users to be able to connect Agency Leaders
  so they has exclusive happy hour to talk shop and share insights.

  Background:
    Given user is on the website home page
    When user select connect agency leaders

  @connectAgencyLeadersSuccessful
  Scenario: User connect agency leaders successful
    When user fill the form to connect agency leaders successful
    Then system show message connect successful

  @ValidateFillMandatory
  Scenario Outline: validate fill mandatory
    When user send form Connect Agency Leaders incomplete "<input>"
    Then System show input form Connect Agency Leaders mandatory
    Examples:
      | input             |
      | first name        |
      | number additional |
      | email             |
      | company           |

  @ValidateFormatInput
  Scenario Outline: validate format input
    When user send invalid input form Connect Agency Leaders incomplete "<idTest>"
    Then System show invalid input form Connect Agency Leaders mandatory message
    Examples:
      | idTest |
      | 2      |
      | 3      |
      | 4      |
