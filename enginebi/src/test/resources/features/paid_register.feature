#Language: en
#Author: Hernan Malave

@RegressionTest
Feature: Paid user registration

  As a Product Owner,
  I want users to be able to register for a paid plan
  So that they can get to know the product

  Background:
    Given user is on the website home page

  @PaidRegister
  Scenario Outline: User paid registers
    Given user selects a registration option paid "<Plan>"
    When user fills out the registration form complete
    Then the system registers the user
    Examples:
      | Plan     |
      | Monthly  |
      | Annual   |
      | Platinum |

  @BasicInformationFillMandatory @Bug
  Scenario Outline: Basic information fill mandatory
    Given user selects a registration option paid "Monthly"
    When user send Basic information form "<input>"
    Then System show button next unavailable
    Examples:
      | input                 |
      | First Name            |
      | Last Name             |
      | Email Address         |
      | Business Phone Number |

  @CompanyInformationFillMandatory @Bug
  Scenario Outline: Company information fill mandatory
    Given user selects a registration option paid "Monthly"
    When user send Company information form "<input>"
    Then System show in form company button next unavailable
    Examples:
      | input        |
      | Company Name |
      | City         |

  @BasicInformationFormatInvalid
  Scenario Outline: basic information format invalid
    Given user select a registration option paid "Monthly" "<idTest>"
    When user send basic information invalid information form
    Then System show invalid format "<message>"
    Examples:
      | idTest | message                                                   |
      | 2      | The input is not valid name or exceeds 50 characters      |
      | 3      | The input is not valid last name or exceeds 50 characters |
      | 4      | The input is not valid name or exceeds 50 characters      |
      | 5      | The input is not valid last name or exceeds 50 characters |
      | 6      | The input is not valid name or exceeds 50 characters      |
      | 7      | The input is not valid last name or exceeds 50 characters |
      | 8      | This email is invalid.                                    |
      | 9      | This email is invalid.                                    |
      | 10     | This email is invalid.                                    |
      | 11     | Please input your phone number!                           |
      | 12     | Please input your phone number!                           |

  @CompanyInformationFormatInvalid
  Scenario Outline: Company information format invalid
    Given user select a registration option paid "Monthly" "<idTest>"
    When user send Company invalid information form
    Then System show invalid format "<message>"
    Examples:
      | idTest | message                                                |
      | 13     | The max allowed length is 50 characters.               |
      | 14     | The input is not a valid city or exceeds 50 characters |
      | 15     | The imput is not a valid company name.                 |
      | 16     | The input is not a valid city or exceeds 50 characters |


