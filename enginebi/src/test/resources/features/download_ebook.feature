#Language: en
#Author: Hernan Malave

@RegressionTest
Feature: user download ebook

  As a Product Owner,
  I want users to be able to download our ebook
  so they can enjoy it

  Background:
    Given user is on the website home page
    When user select download ebook options

  @DownloadSuccessful
  Scenario: User download ebook successful
    When user fill the form ebook options
    Then user choose the format

  @ValidateFillMandatory
  Scenario: validate fill mandatory
    When user doesn't fill the form ebook complete
    Then System show input form ebook download mandatory

  @ValidateFormat
  Scenario Outline: validate format
    When user into email format invalid "<feature>"
    Then input email show invalid format message
    Examples:
      | feature |
      | 3       |
      | 4       |
      | 5       |

