#Language: en
#Author: Hernan Malave

@RegressionTest
Feature: Login

  As a Product Owner,
  I want users have a login button on the website
  So that registered users can sign in

  Background:
    Given user is on the website home page

  @LoginSuccessful
  Scenario: user Login successful
    When user into her data
    Then user makes sign in

  @wrongLogin
  Scenario: user wrong Login
    When user into wrong data
    Then system show message wrong data

  @LoginWrongFormat
  Scenario Outline: user use wrong format at email address
    When user into format invalid "<feature>"
    Then system show message wrong data
    Examples:
      | feature |
      | 3       |
      | 4       |
      | 5       |

  @FillMandatoryLogin
  Scenario Outline: fill mandatory
    When user does not send complete data "<input>"
    Then system show mandatory fill message "<input>"
    Examples:
      | input    |
      | email    |
      | password |



