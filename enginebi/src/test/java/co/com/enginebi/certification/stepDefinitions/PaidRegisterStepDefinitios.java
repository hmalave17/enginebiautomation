package co.com.enginebi.certification.stepDefinitions;

import co.com.enginebi.certification.exceptions.UnexpectedBehavior;
import co.com.enginebi.certification.exceptions.UnexpectedMessage;
import co.com.enginebi.certification.questions.ElementUnavailable;
import co.com.enginebi.certification.questions.ValidateMessage;
import co.com.enginebi.certification.tasks.*;
import co.com.enginebi.certification.utils.AlternativeCase;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.annotations.Managed;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import org.openqa.selenium.WebDriver;

import static co.com.enginebi.certification.enums.ExcelKey.REGISTER_SUITS;
import static co.com.enginebi.certification.enums.SystemKey.USER;
import static co.com.enginebi.certification.enums.SystemMessage.COMPANY_INFORMATION;
import static co.com.enginebi.certification.enums.TestKey.FREE_REGISTER_SUCCESSFUL_TEST;
import static co.com.enginebi.certification.userInterfaces.BasicInformationUserInterface.BASIC_INFORMATION_NEXT_BUTTON;
import static co.com.enginebi.certification.userInterfaces.CompanyInformationUserInterface.*;
import static co.com.enginebi.certification.userInterfaces.HomePageUserInterface.START_PAID_REGISTER_BUTTON;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.CoreMatchers.equalTo;


public class PaidRegisterStepDefinitios {

    @Managed(driver = "Chrome")
    WebDriver herBrowser;
    Actor user =Actor.named(USER.getWords());

    @Given("user selects a registration option paid {string}")
    public void userSelectsARegistrationOptionPaid(String plan) {
        user.can(BrowseTheWeb.with(herBrowser));
        user.attemptsTo(SetDataTest.user(REGISTER_SUITS.getVariable(),FREE_REGISTER_SUCCESSFUL_TEST.getTestNumber()));
        user.attemptsTo(SelectOptionRegister.user(herBrowser, plan, START_PAID_REGISTER_BUTTON));
    }

    @Given("user select a registration option paid {string} {string}")
    public void userSelectARegistrationOptionPaid(String plan, String testCase) {
        user.can(BrowseTheWeb.with(herBrowser));
        user.attemptsTo(SetDataTest.user(REGISTER_SUITS.getVariable(), testCase));
        user.attemptsTo(SelectOptionRegister.user(herBrowser, plan, START_PAID_REGISTER_BUTTON));
    }

    @When("user fills out the registration form complete")
    public void userFillsOutTheRegistrationFormComplete() {
        user.attemptsTo(FillBasicForm.user());
        user.attemptsTo(FillCompanyForm.user());
    }

    @When("user send Basic information form {string}")
    public void userSendBasicInformationForm(String input) {
        user.attemptsTo(FillMandatoryBasicInformation.form(AlternativeCase.basicInformation(input)));
    }

    @When("user send basic information invalid information form")
    public void userSendBasicInformationInvalidInformationForm() {
        user.attemptsTo(FormatInvalidBasicInformation.form());
    }

    @When("user send Company information form {string}")
    public void userSendCompanyInformationForm(String input) {
        user.attemptsTo(FillBasicForm.user());
        user.attemptsTo(FillMandatoryCompanyInformation.form(input));
    }

    @When("user send Company invalid information form")
    public void userSendCompanyInvalidInformationForm() {
        user.attemptsTo(FillBasicForm.user());
        user.attemptsTo(FormatInvalidCompanyInformation.form());
    }

    @Then("the system registers the user")
    public void theSystemRegistersTheUser() {
        user.should(seeThat(ValidateMessage.element(FORM_NAME_LABEL,
                        COMPANY_INFORMATION.getMessage()),
                equalTo(true)).orComplainWith(Error.class, UnexpectedMessage.UNEXPECTED_MESSAGE));
    }

    @Then("System show button next unavailable")
    public void systemShowButtonNextUnavailable() {
        user.should(seeThat(ElementUnavailable.click(BASIC_INFORMATION_NEXT_BUTTON), equalTo(true))
                .orComplainWith(Error.class, UnexpectedBehavior.UNEXPECTED_MESSAGE));
    }

    @Then("System show in form company button next unavailable")
    public void systemShowInFormCompanyButtonNextUnavailable() {
        user.should(seeThat(ElementUnavailable.click(COMPANY_INFORMATION_NEXT_BUTTON), equalTo(true))
                .orComplainWith(Error.class, UnexpectedBehavior.UNEXPECTED_MESSAGE));
    }

    @Then("System show invalid format {string}")
    public void systemShowInvalidFormat(String message) {
        user.should(seeThat(ValidateMessage.element(ERROR_FORMAT_LABEL, message),
                equalTo(true)).orComplainWith(Error.class, UnexpectedMessage.UNEXPECTED_MESSAGE));
        user.should(seeThat(ElementUnavailable.click(COMPANY_INFORMATION_NEXT_BUTTON), equalTo(true))
                .orComplainWith(Error.class, UnexpectedBehavior.UNEXPECTED_MESSAGE));
    }
}
