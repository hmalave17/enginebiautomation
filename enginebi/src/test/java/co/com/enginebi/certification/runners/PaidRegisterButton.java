package co.com.enginebi.certification.runners;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/paid_register.feature",
        glue = {"co.com.enginebi.certification.stepDefinitions", "co.com.enginebi.certification.setup"},
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        monochrome = true
)


public class PaidRegisterButton {
}
