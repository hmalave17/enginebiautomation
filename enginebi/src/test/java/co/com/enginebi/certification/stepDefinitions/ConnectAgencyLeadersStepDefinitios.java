package co.com.enginebi.certification.stepDefinitions;

import co.com.enginebi.certification.exceptions.UnexpectedMessage;
import co.com.enginebi.certification.questions.ValidateMessage;
import co.com.enginebi.certification.tasks.*;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.annotations.Managed;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import org.openqa.selenium.WebDriver;

import static co.com.enginebi.certification.enums.ExcelKey.CONNECT_AGENCY_LEADERS;
import static co.com.enginebi.certification.enums.SystemKey.*;
import static co.com.enginebi.certification.enums.SystemMessage.*;
import static co.com.enginebi.certification.enums.TestKey.CONNECT_AGENCY_LEADERS_TEST;
import static co.com.enginebi.certification.userInterfaces.ConnectAgencyLeadersUserInterface.MANDATORY_FIELD_LABEL;
import static co.com.enginebi.certification.userInterfaces.ConnectAgencyLeadersUserInterface.MESSAGE_CONNECT_AGENCY_LABEL;
import static co.com.enginebi.certification.userInterfaces.HomePageUserInterface.*;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.CoreMatchers.equalTo;

public class ConnectAgencyLeadersStepDefinitios {

    @Managed(driver = "Chrome")
    WebDriver herBrowser;
    Actor user = Actor.named(USER.getWords());

    @When("user select connect agency leaders")
    public void userSelectConnectAgencyLeaders() {
        user.can(BrowseTheWeb.with(herBrowser));
        user.attemptsTo(SetDataTest.user(CONNECT_AGENCY_LEADERS.getVariable(), CONNECT_AGENCY_LEADERS_TEST.getTestNumber()));
        user.attemptsTo(SelectOptionHome.page(SECOND_REFERENCE_TEXT_LABEL, TEXT_REFERENCE.getWords(),
                HOME_PAGE_BIG_BUTTON, TEXT_REFERENCE_II.getWords()));
    }
    @When("user fill the form to connect agency leaders successful")
    public void userFillTheFormToConnectAgencyLeadersSuccessful() {
        user.attemptsTo(FillConnectAgencyLeadersForm.user());
    }

    @When("user send form Connect Agency Leaders incomplete {string}")
    public void userSendFormConnectAgencyLeadersIncomplete(String input) {
        user.attemptsTo(FillMandatoryConnectAgencyLeaders.form(input));
    }

    @When("user send invalid input form Connect Agency Leaders incomplete {string}")
    public void userSendInvalidInputFormConnectAgencyLeadersIncomplete(String feature) {
        user.attemptsTo(SetDataTest.user(CONNECT_AGENCY_LEADERS.getVariable(), feature));
        user.attemptsTo(FormatInvalidConnectAgencyLeaders.form());
    }

    @Then("system show message connect successful")
    public void systemShowMessageConnectSuccessful() {
        user.should(seeThat(ValidateMessage.element(MESSAGE_CONNECT_AGENCY_LABEL, CONNECT_AGENCY_SUCCESSFUL.getMessage()),
                equalTo(true)).orComplainWith(Error.class, UnexpectedMessage.UNEXPECTED_MESSAGE));
    }

    @Then("System show input form Connect Agency Leaders mandatory")
    public void systemShowInputFormConnectAgencyLeadersMandatory() {
        user.should(seeThat(ValidateMessage.element(MANDATORY_FIELD_LABEL, FIELD_MANDATORY.getMessage()),
                equalTo(true)).orComplainWith(Error.class, UnexpectedMessage.UNEXPECTED_MESSAGE));
    }

    @Then("System show invalid input form Connect Agency Leaders mandatory message")
    public void systemShowInvalidInputFormConnectAgencyLeadersMandatoryMessage() {
        user.should(seeThat(ValidateMessage.element(MANDATORY_FIELD_LABEL, INVALID_EMAIL.getMessage()),
                equalTo(true)).orComplainWith(Error.class, UnexpectedMessage.UNEXPECTED_MESSAGE));
    }
}
