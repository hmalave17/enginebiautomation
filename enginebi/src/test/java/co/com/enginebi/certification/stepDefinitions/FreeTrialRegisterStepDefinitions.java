package co.com.enginebi.certification.stepDefinitions;

import co.com.enginebi.certification.exceptions.UnexpectedMessage;
import co.com.enginebi.certification.questions.ValidateMessage;
import co.com.enginebi.certification.tasks.*;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.annotations.Managed;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import org.openqa.selenium.WebDriver;

import static co.com.enginebi.certification.enums.ExcelKey.REGISTER_SUITS;
import static co.com.enginebi.certification.enums.SystemKey.*;
import static co.com.enginebi.certification.enums.SystemMessage.*;
import static co.com.enginebi.certification.enums.TestKey.FREE_REGISTER_SUCCESSFUL_TEST;
import static co.com.enginebi.certification.enums.WorksKey.FREE_TRIAL;
import static co.com.enginebi.certification.userInterfaces.CompanyInformationUserInterface.*;
import static co.com.enginebi.certification.userInterfaces.HomePageUserInterface.START_FREE_REGISTER_BUTTON;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.CoreMatchers.equalTo;

public class FreeTrialRegisterStepDefinitions {


    @Managed(driver = "Chrome")
    WebDriver herBrowser;
    Actor user = Actor.named(USER.getWords());

    @Given("user is on the website home page")
    public void userIsOnTheWebsiteHomePage() {
        user.can(BrowseTheWeb.with(herBrowser));
        user.attemptsTo(GoHomePage.webside());
    }

    @Given("the user selects the free registration option")
    public void theUserSelectsTheFreeRegistrationOption() {
        user.attemptsTo(
                SetDataTest.user(REGISTER_SUITS.getVariable(), FREE_REGISTER_SUCCESSFUL_TEST.getTestNumber()),
                SelectOptionRegister.user(herBrowser, FREE_TRIAL.getWork(), START_FREE_REGISTER_BUTTON)
        );
    }

    @When("the user fills out the registration form")
    public void theUserFillsOutTheRegistrationForm() {
        user.attemptsTo(
                FillBasicForm.user(),
                FillCompanyForm.user()
        );
    }

    @When("user review Subscription Purchase Order Terms")
    public void userReviewSubscriptionPurchaseOrderTerms() {
        user.attemptsTo(
                FillBasicForm.user(),
                VerifyLegalInformation.user(LEGAL_TERMS_BUTTON, SUBSCRIPTION_PURCHASE_ORDER_TERMS.getWords())
        );
    }

    @When("user review Software and Service Terms and Conditions")
    public void userReviewSoftwareAndServiceTermsAndConditions() {
        user.attemptsTo(
                FillBasicForm.user(),
                VerifyLegalInformation.user(LEGAL_TERMS_BUTTON, SOFTWARE_SERVICE_TERMS_CONDITION.getWords())
        );
    }

    @Then("the system registers the user for free")
    public void theSystemRegistersTheUserForFree() {
        user.should(seeThat(ValidateMessage.element(FORM_NAME_LABEL,
                        COMPANY_INFORMATION.getMessage()),
                equalTo(true)).orComplainWith(Error.class, UnexpectedMessage.UNEXPECTED_MESSAGE));
    }

    @Then("System show Subscription Purchase Order Terms information")
    public void systemShowSubscriptionPurchaseOrderTermsInformation() {
        user.should(seeThat(ValidateMessage.element(SUBSCRIPTIONS_BUTTON,
                        LEGAL_INFORMATION_PURCHASE_ORDER.getMessage()),
                equalTo(true)).orComplainWith(Error.class, UnexpectedMessage.UNEXPECTED_MESSAGE));
    }

    @Then("System show Software and Service Terms and Conditions information")
    public void systemShowSoftwareAndServiceTermsAndConditionsInformation() {
        user.should(seeThat(ValidateMessage.element(SOFTWARE_SERVICES_BUTTON,
                        LEGAL_INFORMATION_TERMS_SERVICE.getMessage()),
                equalTo(true)).orComplainWith(Error.class, UnexpectedMessage.UNEXPECTED_MESSAGE));
    }
}
