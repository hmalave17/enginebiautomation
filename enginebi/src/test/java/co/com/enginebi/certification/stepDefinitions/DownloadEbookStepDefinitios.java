package co.com.enginebi.certification.stepDefinitions;

import co.com.enginebi.certification.exceptions.UnexpectedMessage;
import co.com.enginebi.certification.questions.ValidateMessage;
import co.com.enginebi.certification.tasks.*;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.annotations.Managed;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import org.openqa.selenium.WebDriver;

import static co.com.enginebi.certification.enums.ExcelKey.LOGIN_SUCCESSFUL;
import static co.com.enginebi.certification.enums.ExcelKey.REGISTER_SUITS;
import static co.com.enginebi.certification.enums.SystemKey.*;
import static co.com.enginebi.certification.enums.SystemMessage.*;
import static co.com.enginebi.certification.enums.TestKey.FREE_REGISTER_SUCCESSFUL_TEST;
import static co.com.enginebi.certification.userInterfaces.DownloadEbookUserIntarface.ALERT_LABEL;
import static co.com.enginebi.certification.userInterfaces.DownloadEbookUserIntarface.MESSAGE_CHOOSE_FORMAT_LABEL;
import static co.com.enginebi.certification.userInterfaces.HomePageUserInterface.HOME_PAGE_BIG_BUTTON;
import static co.com.enginebi.certification.userInterfaces.HomePageUserInterface.REFERENCE_TEXT_LABEL;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.CoreMatchers.equalTo;

public class DownloadEbookStepDefinitios {

    @Managed(driver = "Chrome")
    WebDriver herBrowser;
    Actor user = Actor.named(USER.getWords());

    @When("user select download ebook options")
    public void userSelectDownloadEbookOptions() {
        user.can(BrowseTheWeb.with(herBrowser));
        user.attemptsTo(SetDataTest.user(LOGIN_SUCCESSFUL.getVariable(), FREE_REGISTER_SUCCESSFUL_TEST.getTestNumber()));
        user.attemptsTo(SelectOptionHome.page(REFERENCE_TEXT_LABEL, NAME_REFERENCE_TEXT_LABEL.getWords(),
                HOME_PAGE_BIG_BUTTON, DOWNLOAD_EBOOK_LABEL.getWords()));
    }

    @When("user fill the form ebook options")
    public void userFillTheFormEbookOptions() {
        user.attemptsTo(FillDownloadEbookForm.user());
    }

    @When("user doesn't fill the form ebook complete")
    public void userDoesnTFillTheFormEbookComplete() {
        user.attemptsTo(FillMandatoryDownloadEbook.input());
    }

    @When("user into email format invalid {string}")
    public void userIntoEmailFormatInvalid(String feature) {
        user.attemptsTo(SetDataTest.user(LOGIN_SUCCESSFUL.getVariable(), feature));
        user.attemptsTo(FormatInvalidDonwloadEbook.form());
    }

    @Then("user choose the format")
    public void userChooseTheFormat() {
        user.should(seeThat(ValidateMessage.element(MESSAGE_CHOOSE_FORMAT_LABEL, CHOOSE_FORMAT_EBOOK.getMessage()),
                equalTo(true)).orComplainWith(Error.class, UnexpectedMessage.UNEXPECTED_MESSAGE));
    }

    @Then("System show input form ebook download mandatory")
    public void systemShowInputFormEbookDownloadMandatory() {
        user.should(seeThat(ValidateMessage.element(ALERT_LABEL,
                        FIELD_MANDATORY.getMessage()),
                equalTo(true)).orComplainWith(Error.class, UnexpectedMessage.UNEXPECTED_MESSAGE));
    }

    @Then("input email show invalid format message")
    public void inputEmailShowInvalidFormatMessage() {
        user.can(BrowseTheWeb.with(herBrowser));
        user.should(seeThat(ValidateMessage.element(ALERT_LABEL,
                        INVALID_EMAIL.getMessage()),
                equalTo(true)).orComplainWith(Error.class, UnexpectedMessage.UNEXPECTED_MESSAGE));
    }
}
