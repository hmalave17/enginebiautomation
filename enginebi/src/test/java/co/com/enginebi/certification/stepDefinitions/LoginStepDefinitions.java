package co.com.enginebi.certification.stepDefinitions;

import co.com.enginebi.certification.exceptions.UnexpectedMessage;
import co.com.enginebi.certification.questions.ValidateMessage;
import co.com.enginebi.certification.tasks.FillMandatoyLogin;
import co.com.enginebi.certification.tasks.Login;
import co.com.enginebi.certification.tasks.SetDataTest;
import co.com.enginebi.certification.utils.AlternativeCase;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.annotations.Managed;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import org.openqa.selenium.WebDriver;

import static co.com.enginebi.certification.enums.ExcelKey.LOGIN_SUCCESSFUL;
import static co.com.enginebi.certification.enums.SystemKey.USER;
import static co.com.enginebi.certification.enums.SystemMessage.*;
import static co.com.enginebi.certification.enums.TestKey.LOGIN_SUCCESSFUL_TEST;
import static co.com.enginebi.certification.enums.TestKey.LOGIN_WRONG_TEST;
import static co.com.enginebi.certification.userInterfaces.LoginPageUserInterface.ALERT_LABEL;
import static co.com.enginebi.certification.userInterfaces.LoginPageUserInterface.WELCOME_LABEL;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.CoreMatchers.equalTo;

public class LoginStepDefinitions {

    @Managed(driver = "Chrome")
    WebDriver herBrowser;
    Actor user = Actor.named(USER.getWords());

    @When("user into her data")
    public void userIntoHerData() {
        user.can(BrowseTheWeb.with(herBrowser));
        user.attemptsTo(SetDataTest.user(LOGIN_SUCCESSFUL.getVariable(), LOGIN_SUCCESSFUL_TEST.getTestNumber()));
        user.attemptsTo(Login.user());
    }

    @When("user into wrong data")
    public void userIntoWrongData() {
        user.can(BrowseTheWeb.with(herBrowser));
        user.attemptsTo(SetDataTest.user(LOGIN_SUCCESSFUL.getVariable(), LOGIN_WRONG_TEST.getTestNumber()));
        user.attemptsTo(Login.user());
    }

    @When("user into format invalid {string}")
    public void userIntoFormatInvalid(String feature) {
        user.can(BrowseTheWeb.with(herBrowser));
        user.attemptsTo(SetDataTest.user(LOGIN_SUCCESSFUL.getVariable(), feature));
        user.attemptsTo(Login.user());
    }

    @When("user does not send complete data {string}")
    public void userDoesNotSendCompleteData(String input) {
        user.can(BrowseTheWeb.with(herBrowser));
        user.attemptsTo(SetDataTest.user(LOGIN_SUCCESSFUL.getVariable(), LOGIN_SUCCESSFUL_TEST.getTestNumber()));
        user.attemptsTo(FillMandatoyLogin.input(AlternativeCase.login(input)));
    }

    @Then("user makes sign in")
    public void userMakesSignIn() {
        user.should(seeThat(ValidateMessage.element(WELCOME_LABEL, LOGIN_WELCOME.getMessage()),
                equalTo(true)).orComplainWith(Error.class, UnexpectedMessage.UNEXPECTED_MESSAGE));
    }

    @Then("system show message wrong data")
    public void systemShowMessageWrongData() {
        user.should(seeThat(ValidateMessage.element(ALERT_LABEL, LOGIN_WRONG.getMessage()),
                equalTo(true)).orComplainWith(Error.class, UnexpectedMessage.UNEXPECTED_MESSAGE));
    }

    @Then("system show mandatory fill message {string}")
    public void systemShowMandatoryFillMessage(String input) {
        user.should(seeThat(ValidateMessage.element(ALERT_LABEL,
                        String.format(LOGIN_MANDATORY_FILL.getMessage(), AlternativeCase.loginInputName(input))),
                equalTo(true)).orComplainWith(Error.class, UnexpectedMessage.UNEXPECTED_MESSAGE));
    }
}
