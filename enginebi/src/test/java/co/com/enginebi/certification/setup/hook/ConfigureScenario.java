package co.com.enginebi.certification.setup.hook;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import net.serenitybdd.annotations.Managed;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.model.environment.SystemEnvironmentVariables;
import org.openqa.selenium.WebDriver;

import static co.com.enginebi.certification.enums.SystemKey.URL_HOME_PAGE;
import static co.com.enginebi.certification.enums.SystemMessage.URL_DOESNOT_DEFINE;
import static co.com.enginebi.certification.enums.SystemKey.USER;

public class ConfigureScenario {

    @Managed(driver = "chrome")
    WebDriver herBrowser;
    Actor user = Actor.named(USER.getWords());

    @Before
    public void start() {
        OnStage.setTheStage(new OnlineCast());
        user.can(BrowseTheWeb.with(herBrowser));

        String homePageUrl = SystemEnvironmentVariables.createEnvironmentVariables().getProperty(URL_HOME_PAGE.getWords());
        if (homePageUrl == null || homePageUrl.isEmpty()) {
            throw new RuntimeException(URL_DOESNOT_DEFINE.getMessage());
        }
        user.attemptsTo(net.serenitybdd.screenplay.actions.Open.url(homePageUrl));
    }

    @After
    public void finalize() {
        OnStage.drawTheCurtain();
    }

}
