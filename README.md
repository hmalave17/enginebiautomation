Proyecto: Enginebi test Automatización
El proyecto de Automatización Enginebi test se elabora con la finalidad de evaluar los conocimientos en el área de Automatización de Hernan José Malave Montero, el cual se encuentra interesado ser parte de la compañia Enginebi.

Este proyecto cuenta con las siguientes características:

Lenguaje de programación: Java 11.0.18
Framework de Automatización: Serenity
Patrón de diseño: Screenplay
Framework de soporte BDD (Behavior development driver): Cucumber
Lenguaje de BDD (Behavior development driver): Gherkin
Constructor de proyecto: Gradle 7.0

Nota: El proyecto esta diseñado para ser ejecutado en maquinas Windows y navegador Chrome.

El proyecto posee la siguiente estructura:


Documentation: En esta carpeta se van encontrar los siguientes archivos:

Test Summary.docx - Archivo que contiene el resumen de las novedades encontradas durante la ejecución de las pruebas

Test Case EngineBI.xlsx - El cual posee los test case solicitados en la prueba.

Evidence: Carpeta la cual tiene las evidencias de los bugs encontrados durante la ejecuciónd del plan de pruebas.

Evidencias.docx - Archivo que contiene evidencias en imagenes de los bugs encontrados.

VideoUno.mp4 - Evidencia en video de los bugs.

VideoDos.mp4 - Evidencia en video de los bugs.


Carpeta enginebi:

 src/main/java/co/com/enginebi/certification/enums: En este paquete encontramos las palabras claves con el fin de evitar dejar código estático en nuestra automatización

 src/main/java/co/com/enginebi/certification/interactions: Este paquete maneja todas las funciones de iteraciones con elementos web, por ejemplo, realizar click en un elemento web, escribir sobre un elemento web, entre otros.

 src/main/java/co/com/enginebi/certification/models: Este paquete que gestiona la data necesaria para ejecutar las pruebas automatizadas.

 src/main/java/co/com/enginebi/certification/tasks: Este paquete maneja las tareas que va realizar el usuario para cumplir con lo esperado en la prueba automatizada, asi cumpliendo con el Patrón de diseño Screenplay.

 src/main/java/co/com/enginebi/certification/userinterface: En este paquete referenciamos los elementos de las vistas con las que vamos interactuar, asi garantizando la reusabilidad de código.
 
 src/main/java/co/com/enginebi/certification/utils: En este paquete tenemos los archivos que nos ayudan a ejecutar la automatización. 

 src/test/java/co/com/enginebi/certification/runners: Este paquete contiene los archivos runner los cuales estan encargados de ejecutar nuestra prueba.

 src/test/java/co/com/enginebi/certification/setup: Este paquete contiene los archivos con los que se parametrizan las condiciones iniciales de la prueba. 

 src/test/java/co/com/enginebi/certification/stepDefinitions: Este paquete contiene nuestros archivos de stepsDefinitions los cuales definen el paso a paso del lado de lógica de programación

src/test/resources/datatables: Este paquete contiene la data de los diferentes escenarios de la prueba. 

 src/test/resources/features: Este paquete contiene nuestros archivos feature los cuales definen el escenario a evaluar del lado del BDD


 gitignore: El archivo encargado de bloquear subir archivos no necesarios en nuestro repositorio


 serenity.properties: Es el archivo que nos ayuda a setear propiedades del Framework de Serenity



Datos adicionales del proyecto:

Se implemento la anotación "@share" de serenity para compartir data entre diferentes contextos.
Se implemento tags para todos los diferentes escenarios automatizados.
Se implemento tag generico llamado "@RegressionTest" para poder ejecutar toda la suite de pruebas.
Se implemento tag generico llamado "@Bug" para identificar los casos en que se encontraron Bugs.


Pasos para obtener el proyecto de automation:

Clonar el repositorio en la ubicación deseada, usando el comando: git clone https://gitlab.com/hmalave17/enginebiautomation

Ingresar a la carpeta enginebi


Pasos para ejecutar el proyecto

Se debe primero realizar los pasos de la sección "Pasos para obtener el proyecto de automation"
Opción 1: Abrir el proyecto en el IDE de su preferencia que soporte Java, ejecutar el archivo runner de su preferencia.
Opción 2: Abrir el proyecto en el IDE de su preferencia que soporte Java, ejecutar el archivo "TestRunner" que está configurado con el tag @RegressionTest para ejecutar todas las pruebas.
Opcion 3: Abrir desde la consola de windows la carpeta donde esta clonado el proyecto, la consola debe tener permisos de administrador, ejecutar el comando "gradle w test"


Generación de reporte del framework

El reporte se generará en la carpeta /target/site/serenity abriendo el archivo index.html